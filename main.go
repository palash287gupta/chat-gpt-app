package main

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/palash287gupta/chat-gpt-app/config"
	"gitlab.com/palash287gupta/chat-gpt-app/gpt"
)

func main() {
	appConfig := config.LoadEnvDetails()
	apiKey := appConfig.GetString("API_KEY")
	if apiKey == "" {
		log.Fatal("Missing API KEY")
	}

	client := gpt.NewClient(apiKey)
	cmd := &cobra.Command{
		Short: "Ask your questions to gpt",
		Run: func(cmd *cobra.Command, args []string) {
			scanner := bufio.NewScanner(os.Stdin)
			quit := false

			for !quit {
				fmt.Print("Write your question or quit to end the chat: ")
				if !scanner.Scan() {
					break
				}
				question := scanner.Text()
				if question == "quit" {
					quit = true
				} else {
					gpt.GetAnswer(client, context.Background(), question)
				}
			}
		},
	}
	cmd.Execute()
}
