package gpt

import (
	"context"
	"fmt"
	"log"

	"github.com/PullRequestInc/go-gpt3"
)

const (
	engine   = gpt3.TextDavinci003Engine
	temp     = 0.5
	maxToken = 3000
)

func NewClient(apiKey string) gpt3.Client {
	return gpt3.NewClient(apiKey)
}

func GetAnswer(client gpt3.Client, ctx context.Context, question string) {
	req := gpt3.CompletionRequest{
		Prompt:      []string{question},
		MaxTokens:   gpt3.IntPtr(maxToken),
		Temperature: gpt3.Float32Ptr(temp),
	}

	err := client.CompletionStreamWithEngine(ctx, engine, req, onResponseRceived)
	if err != nil {
		log.Printf("error getting answer for question %v: %v", question, err)
	}
	fmt.Print("\n")
}

func onResponseRceived(resp *gpt3.CompletionResponse) {
	fmt.Print(resp.Choices[0].Text)
}
