package config

import (
	"log"

	"github.com/spf13/viper"
)

func LoadEnvDetails() *viper.Viper {
	var config = viper.New()
	config.SetConfigFile(".env")
	if err := config.ReadInConfig(); err != nil {
		log.Fatal("error reading config file: ", err)
	}
	return config
}
