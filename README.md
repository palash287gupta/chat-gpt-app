# chat-gpt-app

- go the directory chat-gpt-app.

- run `go mod tidy`

- paste your own api key in env file.

- if you don't have api key, create a new one using <https://beta.openai.com/account/api-keys>

- run `go run main.go`
